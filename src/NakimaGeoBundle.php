<?php
declare(strict_types=1);
namespace Nakima\GeoBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NakimaGeoBundle extends Bundle
{
}
