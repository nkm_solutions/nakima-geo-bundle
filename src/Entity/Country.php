<?php
declare(strict_types=1);
namespace Nakima\GeoBundle\Entity;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Nakima\CoreBundle\Entity\BaseStatusEntity;

/**
 * @MappedSuperclass
 */
class Country extends BaseStatusEntity
{

    /**
     * @Column(type="string", length=2, unique=true, nullable=false)
     */
    protected $code;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
    }

    public function __toString()
    {
        return "[$this->code] $this->name";
    }

    public function toArray($options = [])
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
        ];
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

}
